extern crate getopts;

pub fn run() {
    let args: Vec<String> = ::std::env::args().collect();

    // If no arguments, then interactive mode
    if args.len() <= 1 {
        interactive_mode();
        return;
    }

    // Parse Command Line Arguments
    let program_name = args[0].clone();

    let mut opts = getopts::Options::new();
    opts.optflag("h", "help", "print this help menu");
    let matches = match opts.parse(&args[1..]) {
        Ok(m) => { m }
        Err(f) => { panic!(f.to_string()) }
    };

    // Decide on program action
    if matches.opt_present("h") {
        print_usage(&program_name, &opts);
        return;
    }
}

fn interactive_mode() {
    println!("Hi, sly guy");

    let mut input = String::new();
    while let Ok(_bytes_read) = ::std::io::stdin().read_line(&mut input) {
        match input.trim() {
            "q" => return,
            "quit" => return,
            _ => println!("Unknown command"),
        };

        input.clear();
    };
}

fn print_usage(program_name: &String, opts: &getopts::Options ) {
    let brief = format!("Usage: {} [options]
Runs in interactive mode if no options are passed",
                        program_name);
    print!("{}", opts.usage(&brief));
}

